/*********************************************
 * OPL 12.6.0.0 Model
 * Author: ACER
 * Creation Date: 30/08/2016 at 15:48:05
 *********************************************/

//constantes
int SD = ...;
int ENLACES = ...;
int VERTICES = ...;
range R = 1..3;
range sd = 1..SD;
range vertices = 1..VERTICES;
range mn = 1..ENLACES;
int alfa[sd][R]=...;
//int SOLBIN[vertices][1..2]=...;
int SOLICITUDES[sd][1..2]=...;
float Lmax[R] = ...;
int F = ...;
int GB = ...;
float L[mn] = ...;
int NE[vertices][mn]=...;
int NE1[vertices][mn]=...;
int NE2[vertices][mn]=...;

//variables
dvar int+ MI[mn]; //8 enlaces
dvar int X[sd][sd] in 0..1; // 12 * 12 = 144
dvar int+ N[sd][R]; // 12 * 3 = 36
dvar int+ E[sd]; // 12
dvar int+ S[sd]; // 12
dvar int+ NFS[sd]; // 12
dvar int+ Z[sd][R]; // 12 * 3 = 36
dvar int+ NL[sd][R][mn]; // 12 * 3 * 8 = 288
dvar int Y[sd][R][mn] in 0..1; // 12 * 3 * 8 = 288

dvar float c;

minimize c;

subject to {
        forall(mn1 in mn) { // 8
                //c >= MI[mn1];
                c >= MI[mn1]+1;
        }

        /* 1- Single path routing constraint */
        forall(s_d in sd) { // 12
                        ctSinglePathRouting2:
                        sum(r in R, m_n in mn: NE1[SOLICITUDES[s_d][1]][m_n] == 1)
Y[s_d][r][m_n] == 1;
        }
        
        forall(s_d in sd) {//12
                        ctSinglePathRouting2_2:
                        sum(r in R,m_n in mn: NE2[SOLICITUDES[s_d][1]][m_n] == 1)
Y[s_d][r][m_n] == 0;
        }

        forall(s_d in sd) {//12
                        ctSinglePathRouting3:
                        sum(r in R,m_n2 in mn: NE2[SOLICITUDES[s_d][2]][m_n2] == 1)
Y[s_d][r][m_n2] == 1;
        }
        
        forall(s_d in sd) {//12
                        ctSinglePathRouting3_2:
                        sum(r in R, m_n2 in mn: NE1[SOLICITUDES[s_d][2]][m_n2] == 1)
Y[s_d][r][m_n2] == 0;
        }
       
        
        
//        forall(s_d in sd, i in vertices) {
//                        ctSinglePathRouting4:
//                        sum(r in R) PN[s_d][r][i] <= 1;
//        }

        forall(s_d in sd, i in vertices, r in R) {//12*4*3=144
                                
                	if(i != SOLICITUDES[s_d][2] && i != SOLICITUDES[s_d][1]) {
                        ctSinglePathRouting4_1:
                        (sum(m_n1 in mn: NE1[i][m_n1] == 1) Y[s_d][r][m_n1]) - 
                        (sum(m_n2 in mn: NE2[i][m_n2] == 1) Y[s_d][r][m_n2]) == 0;
                    } 
                                        
        } 
        
        forall(s_d in sd, i in vertices, r in R) {//12*4*3*2=288  
        	ctSinglePathRouting4_2:
            (sum(m_n1 in mn: NE1[i][m_n1] == 1) Y[s_d][r][m_n1]) <= 1;
            (sum(m_n2 in mn: NE2[i][m_n2] == 1) Y[s_d][r][m_n2]) <= 1; 
                                        
        } 
        

//        forall(s_d in sd, r in R, i in vertices, j in vertices) {
//
//                if(Adj[i][j] > 0) {
//                ctSinglePathRouting5_1:
//
//                        PN[s_d][r][i] + PN[s_d][r][j] >= 2 * Y[s_d][r][Adj[i][j]];
//
//                }
//        } 

        /* 2- Flow conservation constraint */


        forall(s_d in sd) {//12*3=36
                forall(r in R) {
                        ctFlowConservationConstraint1:
                        sum(m_n in mn: NE1[SOLICITUDES[s_d][1]][m_n] == 1) NL[s_d][r][m_n]
== N[s_d][r];
                }
        }
        
        forall(s_d in sd) {//12*3=36
                forall(r in R) {
                        ctFlowConservationConstraint1_2:
                        sum(m_n in mn: NE2[SOLICITUDES[s_d][1]][m_n] == 1) NL[s_d][r][m_n]
== 0;
                }
        }

        forall(s_d in sd) {//12*3=36
                forall(r in R) {
                        ctFlowConservationConstraint2:
                        sum(m_n in mn: NE2[SOLICITUDES[s_d][2]][m_n] == 1) NL[s_d][r][m_n] == N[s_d][r];
                }
        }
        
        forall(s_d in sd) {//12*3=36
                forall(r in R) {
                        ctFlowConservationConstraint2_2:
                        sum(m_n in mn: NE1[SOLICITUDES[s_d][2]][m_n] == 1) NL[s_d][r][m_n] == 0;
                }
        }
        
        
		
//        forall(s_d in sd, r in R, i in vertices: i != SOLICITUDES[s_d][1] && i != SOLICITUDES[s_d][2]) {
//			ctFlowConservationConstraint3:
//                (sum(m_n in mn: NE[i][m_n] == 1) NL[s_d][r][m_n]) == (2 * N[s_d][r]);
//        } 
        

 		forall(s_d in sd, i in vertices,r in R) {//12*4*3=144
                 
                	if(i != SOLICITUDES[s_d][2] && i != SOLICITUDES[s_d][1]) {
                        ctSinglePathRouting4_1_2:
                        sum(m_n in mn: NE1[i][m_n] == 1) NL[s_d][r][m_n] -
                        sum(m_n in mn: NE2[i][m_n] == 1) NL[s_d][r][m_n] == 0;
                    }                     
        } 
        /* 3- Starting Frequencies ordering constraint */

        forall(t in sd){//12*12=144
                forall(u in sd){
                        if(t != u){
                                ctStartingFrequenciesOrderingConstraint:
                                X[t][u] + X[u][t] == 1;
                        }
                }
        }
//
////        /* 4- Spectrum continuity and non-overlapping spectrum
////           allocation constraints */
        forall(t in sd){//12*12*8*3*3=10368
                forall(u in sd){
                        forall(m_n in mn, r in R, r2 in R){
                                if(t != u)
                                        ctSpectrumContinuity:
                                        E[u] - S[t] <= F * (X[t][u] + 2 - Y[t][r][m_n] - Y[u][r2][m_n]) - GB - 1;
                        }
                }
        }
//
//        /* 5- Physical Link Usage Constraint */
//
        forall(s_d in sd, r in R, mn in mn){//12*3*8=288
                ctPhysicalLinkUsage:
                NL[s_d][r][mn] <= Y[s_d][r][mn] * F;
        }
//
////        /* 6- Spectrum path Modulation Format constraint */
////
        forall(s_d in sd){//12*3=36
                forall(r in R){
                        ctSpectrumPathModulation:
                        N[s_d][r] <= Z[s_d][r] * F;
                }
        }
//
//        /* 7- Modulation Format Constraint */
//
        forall(s_d in sd) {//12
                ctModulationFormatConstraint:
                sum(r in R) Z[s_d][r] == 1;
        };


        /* 8- Transmission reach constraint */
        forall(s_d in sd, r in R){//12*3=36
                ctTransmissionReachConstraint:
                sum(mn2 in mn) (Y[s_d][r][mn2] * L[mn2]) <= Lmax[r];
        }

        /* 9- Max_FS_ID Constraint */
        forall(mn1 in mn) {//8*12*3=288
        	forall(s_d in sd){
        		forall(r in R) {
        	  
	                ctMax_FS_IDConstraint:
	                //MI[mn] >= E[s_d] - F * (1 - Y[s_d][r][mn]);
	                MI[mn1] >= E[s_d] - F * (1 - Y[s_d][r][mn1]);
	             }
            }                            
        }

//        /* 10- Others... */

        forall(s_d in sd){//12*3=36
                forall(r in R){
                        ctOthers01:
                        N[s_d][r] == Z[s_d][r] * alfa[s_d][r];
                }
        }

        forall(s_d in sd){//12
                ctOthers02:
                E[s_d] == S[s_d] + NFS[s_d] - 1;
        }

        forall(s_d in sd){//12
                ctOthers03:
                NFS[s_d] == sum(r in R) N[s_d][r];
        }
}