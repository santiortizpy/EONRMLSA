/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jgrapht.ilp;

/**
 *
 * @author Ivan
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.KShortestPaths;
import org.jgrapht.graph.*;
import static org.jgrapht.ilp.SLILP.isNumeric;

public class SPILP {

    public static void main(String args[]) throws IOException {

        long time_start1, time_end1, time_start2, time_end2;
        
        FileReader fsolicitudes;
        FileReader fr;
        
        capaVirtual();//Metodo que creó Santi...

        Configuracion config = leerConfiguraciones();
        // Definimos e instanciamos el grafo con pesos
        SimpleDirectedGraph<String, DefaultWeightedEdge> directedGraph =
                new SimpleDirectedGraph(DefaultWeightedEdge.class);
        cargarGrafo(directedGraph);
        String alfa = leerSolicitudes(directedGraph, config.getK());
        fsolicitudes = new FileReader("solicitudesprueba.txt");
        int cantSolicitudes = 0;
        
        try (BufferedReader entrada = new BufferedReader(fsolicitudes)) {

            while (entrada.readLine() != null) {
                cantSolicitudes++;
            }
        }
        fsolicitudes.close();
        fr = new FileReader("edgesprueba.txt");
        int cantEnlaces = 0;
        try (BufferedReader entrada = new BufferedReader(fr)) {


            while (entrada.readLine() != null) {
                cantEnlaces++;
            }
        }


        FileWriter fw = new FileWriter("./src/SPILP_1/SP-ILP_1.dat");

        BufferedWriter bw = new BufferedWriter(fw);
        try (PrintWriter salida = new PrintWriter(bw)) {
            System.out.println("K = " + config.getK() + ";");
            salida.println("K = " + config.getK() + ";");
            System.out.println("SD = " + cantSolicitudes + ";");
            salida.println("SD = " + cantSolicitudes + ";");
            System.out.println("E = " + cantEnlaces + ";");
            salida.println("E = " + cantEnlaces + ";");
            System.out.println("G = " + config.getG() + ";");
            salida.println("G = " + config.getG() + ";");
            System.out.println("alfa = " + alfa);
            salida.println("alfa = " + alfa);


            ArrayList<ArrayList<String>> listaTotalCaminos = leerTotalCaminos();
            Set<DefaultWeightedEdge> listaTotalEnlaces = directedGraph.edgeSet();
            //ArrayList<ArrayList<String>> listaTotalCaminos = directedGraph.edgeSet();

            System.out.println("R = [");
            salida.println("R = [");


            for (Iterator<ArrayList<String>> it4 = listaTotalCaminos.iterator(); it4.hasNext();) {
                String caminoComparador = it4.next().toString();
                //System.out.println("caminoComparador = " + caminoComparador + "\n");
                System.out.print("[");
                salida.print("[");
                for (Iterator<DefaultWeightedEdge> it = listaTotalEnlaces.iterator(); it.hasNext();) {
                    String enlaceAux = it.next().toString();
                    String enlace = enlaceAux.substring(1, enlaceAux.length() - 1);
                    //System.out.println("enlace = " + enlace + "\n");

                    String result;
                    if (caminoComparador.toString().contains(enlace.toString())) {
                        result = "1";
                    } else {
                        result = "0";
                    }
                    /*for (String enlace : camino) {
                     //System.out.println("enlace = " + enlace + "\n");
                     if (caminoComparador.contains(enlace)) 
                     result = "1";
                     else
                     result = "0";

                     }*/
                    if (it.hasNext()) {
                        System.out.print(result + ",");
                        salida.print(result + ",");
                    } else {
                        System.out.print(result);
                        salida.print(result);
                    }
                }
                if (it4.hasNext()) {
                    System.out.println("],");
                    salida.println("],");
                } else {
                    System.out.println("]];");
                    salida.println("]];");
                }
            }
            salida.close();
        }



        /* Probamos la conexión de Java con CPLEX */
        String argumentos[] = {"-v", "./src/SPILP_1/SP-ILP_1.mod", "./src/SPILP_1/SP-ILP_1.dat", "salidaCplexSP-ILP_1.txt"};
        
        time_start1 = System.currentTimeMillis();
                
        OplRunILP.main(argumentos);
        
        time_end1 = System.currentTimeMillis();
        
        System.out.println("La cota de la ILP 2 ha tomado " + (time_end1 - time_start1) + " milisegundos");

        fr = new FileReader("kshortestpathCompleto.txt");
        int cantTotalCaminos = 0;
        try (BufferedReader entrada = new BufferedReader(fr)) {


            while (entrada.readLine() != null) {
                cantTotalCaminos++;
            }
        }

        ArrayList<Integer> indiceCaminosElegidos = leerSalidaCplex(config.getK(), cantTotalCaminos, cantSolicitudes);

        System.out.println("indiceCaminosElegidos.size() = " + indiceCaminosElegidos.size());
        for (int x = 0; x < indiceCaminosElegidos.size(); x++) {
            System.out.println(indiceCaminosElegidos.get(x));
        }

        ArrayList<ArrayList<String>> caminosElegidos = leerSoloCaminosElegidos(indiceCaminosElegidos);

        for (int x = 0; x < caminosElegidos.size(); x++) {
            System.out.println(caminosElegidos.get(x));
        }

        String alfaSegundaFase = leerSolicitudesSegundaFase(directedGraph, indiceCaminosElegidos);

        FileWriter fw2 = new FileWriter("./src/SPILP_2/SP-ILP_2.dat");

        BufferedWriter bw2 = new BufferedWriter(fw2);
        try (PrintWriter salida2 = new PrintWriter(bw2)) {
            System.out.println("K = " + config.getK() + ";");
            salida2.println("K = " + config.getK() + ";");
            System.out.println("SD = " + cantSolicitudes + ";");
            salida2.println("SD = " + cantSolicitudes + ";");
            System.out.println("Ftotal = " + config.getfTotal() + ";");
            salida2.println("Ftotal = " + config.getfTotal() + ";");
            System.out.println("G = " + config.getG() + ";");
            salida2.println("G = " + config.getG() + ";");
            System.out.println("alfa = " + alfaSegundaFase);
            salida2.println("alfa = " + alfaSegundaFase);


            System.out.println("l = [");
            salida2.println("l = [");
            for (Iterator<ArrayList<String>> it = caminosElegidos.iterator(); it.hasNext();) {
                ArrayList<String> camino = it.next();
                System.out.print("[");
                salida2.print("[");
                for (Iterator<ArrayList<String>> it4 = caminosElegidos.iterator(); it4.hasNext();) {
                    ArrayList<String> caminoComparador = it4.next();
                    String result = "0";
                    for (String enlace : camino) {

                        if (caminoComparador.contains(enlace)) {
                            result = "1";
                            break;
                        }

                    }
                    if (it4.hasNext()) {
                        System.out.print(result + ",");
                        salida2.print(result + ",");
                    } else {
                        System.out.print(result);
                        salida2.print(result);
                    }
                }
                if (it.hasNext()) {
                    System.out.println("],");
                    salida2.println("],");
                } else {
                    System.out.println("]];");
                    salida2.println("]];");
                }
            }
        }

        /* Probamos la conexión de Java con CPLEX */
        String argumentos2[] = {"-v", "./src/SPILP_2/SP-ILP_2.mod", "./src/SPILP_2/SP-ILP_2.dat", "salidaCplexSP-ILP_2.txt"};
        
        time_start2 = System.currentTimeMillis();
        OplRunILP.main(argumentos2);

        time_end2 = System.currentTimeMillis();
        
        System.out.println("La fase 2 de ILP 2 ha tomado " + (time_end2 - time_start2) + " milisegundos");
        System.out.println("EL ILP 2 ha tomado " + (time_end1 - time_start1 + time_end2 - time_start2) + " milisegundos");
        
        String f = leerArchivo("salidaCplexSP-ILP_2.txt");
        f = f.substring(f.indexOf("f")+5, f.indexOf("]"));
        String [] t = f.split(" ");
        System.out.println(t[0] + "-" + t[1] + "-" +  t[2]);
        
         FileReader nodos = new FileReader("solicitudesprueba.txt");
         String nodosstr = "";
            BufferedReader n = new BufferedReader(nodos);
                String line;
                while ((line = n.readLine()) != null) {
                    nodosstr += line + "\n";
                }
        String [] lineas = nodosstr.split("\n");
        String [] inicio = new String[t.length];
        String [] fin = new String[t.length];;
        
        int cont = 0;
        for (String temp: lineas){
            String [] temp2;
            temp2 = temp.split("\t");
            inicio[cont] = temp2[0];
            fin[cont]= temp2[1];
            cont++;
        }
        alfaSegundaFase = alfaSegundaFase.substring(1,alfaSegundaFase.length()-2);
        String[] alfax = alfaSegundaFase.split(",");
        
        for(int i=0; i<t.length; i++){
           
            System.out.println("Nodo inicio: " + inicio[i] + " slot utilizado: [" + t[i]+": "+ alfax[i] + "] Nodo final:" + fin[i]);
        }
        
    }
    
    private static void cargarGrafo(SimpleDirectedGraph<String, DefaultWeightedEdge> directedGraph) {
        try {

            FileReader fr = new FileReader("edgesprueba.txt");
            String[] edgeText;
            DefaultWeightedEdge ed;

            try (BufferedReader entrada = new BufferedReader(fr)) {
                String line;
                while ((line = entrada.readLine()) != null) {
                    edgeText = line.split("\t");
                    directedGraph.addVertex(edgeText[0]);
                    directedGraph.addVertex(edgeText[1]);
                    ed = directedGraph.addEdge(edgeText[0], edgeText[1]);
                    directedGraph.setEdgeWeight(ed, Double.parseDouble(edgeText[2]));
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Archivo no encontrado: " + ex);
            Logger
                    .getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void calcularCaminosMasCortos(SimpleDirectedGraph<String, DefaultWeightedEdge> directedGraph, String origen, String destino, int contadorEscritura, int k) {
        try {
            int cont = 0;
            FileWriter fw = new FileWriter("kshortestpath.txt");
            FileWriter fw2;
            if (contadorEscritura == 0) {
                fw2 = new FileWriter("kshortestpathCompleto.txt");
            } else {
                fw2 = new FileWriter("kshortestpathCompleto.txt", true);
            }
            BufferedWriter bw = new BufferedWriter(fw);
            BufferedWriter bw2 = new BufferedWriter(fw2);
            try (PrintWriter salida = new PrintWriter(bw); PrintWriter salida2 = new PrintWriter(bw2)) {

                KShortestPaths caminosCandidatos = new KShortestPaths(directedGraph, origen, k);

                System.out.println("K-shortest path de " + origen + " a " + destino + " es ");

                List<GraphPath<String, DefaultEdge>> paths = caminosCandidatos.getPaths(destino);

                int contAux = 0;
                for (Iterator<GraphPath<String, DefaultEdge>> it = paths.iterator(); it.hasNext();) {
                    contAux = contAux + 1;
                    GraphPath<String, DefaultEdge> path = it.next();
                    System.out.print("Camino " + ++cont + ": ");
                    for (Iterator<DefaultEdge> it2 = path.getEdgeList().iterator(); it2.hasNext();) {
                        DefaultEdge edge = it2.next();
                        System.out.print("<" + edge + ">\t");
                        salida.print("<" + edge + ">\t");
                        salida2.print(edge + "\t");
                    }
                    System.out.println(": " + path.getWeight());
                    salida.println(": \t" + path.getWeight());
                    salida2.println(": \t" + path.getWeight());
                }

                if (contAux < k) {
                    while (contAux < k) {
                        Iterator<GraphPath<String, DefaultEdge>> it = caminosCandidatos.getPaths(destino).iterator();
                        contAux = contAux + 1;
                        GraphPath<String, DefaultEdge> path = it.next();
                        System.out.print("Camino " + ++cont + ": ");
                        for (Iterator<DefaultEdge> it2 = path.getEdgeList().iterator(); it2.hasNext();) {
                            DefaultEdge edge = it2.next();
                            System.out.print("<" + edge + ">\t");
                            salida.print("<" + edge + ">\t");
                            salida2.print(edge + "\t");
                        }
                        System.out.println(": " + path.getWeight());
                        salida.println(": \t" + path.getWeight());
                        salida2.println(": \t" + path.getWeight());
                    }
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static String leerSolicitudes(SimpleDirectedGraph<String, DefaultWeightedEdge> directedGraph, int k) {

        final double C = 154;

        ArrayList<Parametro> listaParams = leerParametros();

        String vectorAlfa = "[[";

        ArrayList<Solicitud> listaSolicitudes = new ArrayList();
        try {

            FileReader fr = new FileReader("solicitudesprueba.txt");
            String[] edgeText;

            try (BufferedReader entrada = new BufferedReader(fr)) {
                String line;
                while ((line = entrada.readLine()) != null) {
                    edgeText = line.split("\t");
                    Solicitud sol = new Solicitud(edgeText[0], edgeText[1], Double.parseDouble(edgeText[2]));
                    listaSolicitudes.add(sol);
                }
            }

            Double ftotal = 0.0;


            int contadorEscritura = 0;
            for (Iterator<Solicitud> it = listaSolicitudes.iterator(); it.hasNext();) {
                Solicitud solicitud = it.next();

                System.out.print("Origen: " + solicitud.getOrigen() + "\t"
                        + "Destino:" + solicitud.getDestino() + "\t"
                        + "FS requerida:" + solicitud.getVelocidad() + "\n");
                calcularCaminosMasCortos(directedGraph, solicitud.getOrigen(), solicitud.getDestino(), contadorEscritura, k);
                ArrayList<Double> listaDistancias = leerDistancias();
                Double tsd;
                int cont = 0;
                for (Iterator<Double> it2 = listaDistancias.iterator(); it2.hasNext();) {
                    Double dist = it2.next();
                    System.out.println("Distancia de camino " + ++cont + ": " + dist);
                    int nivelModulacionCorrespondiente = leerNivelModulacionCorrespondiente(dist, listaParams);
                    System.out.println("Nivel de Modulación correspondiente es " + nivelModulacionCorrespondiente);
                    System.out.println("alfa = " + Math.ceil(solicitud.getVelocidad() * C / (nivelModulacionCorrespondiente * C)));
                    Double d = Math.ceil(solicitud.getVelocidad() * C / (nivelModulacionCorrespondiente * C));

                    if (it.hasNext()) {
                        if (it2.hasNext()) {
                            vectorAlfa = vectorAlfa + d.intValue() + ",";
                        } else {
                            vectorAlfa = vectorAlfa + d.intValue() + "],[";
                        }
                    } else if (it2.hasNext()) {
                        vectorAlfa = vectorAlfa + d.intValue() + ",";
                    } else {
                        vectorAlfa = vectorAlfa + d.intValue();
                    }
                    tsd = Math.ceil(solicitud.getVelocidad() * C / C);
                    System.out.println("Tsd = " + tsd);
                    ftotal += tsd;
                }
                contadorEscritura++;
            }

            vectorAlfa = vectorAlfa + "]];";

            //System.out.println("vectorAlfa = " + vectorAlfa);
            System.out.println("ftotal = " + ftotal / C + ";");
            //Sumatoria de todos los Tsd de los caminos / C 
   
        } catch (FileNotFoundException ex) {
            System.out.println("Archivo no encontrado: " + ex);
            Logger
                    .getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        }


        return vectorAlfa;
    }

    private static String leerSolicitudesSegundaFase(SimpleDirectedGraph<String, DefaultWeightedEdge> directedGraph, ArrayList<Integer> indiceCaminosElegidos) {

        final double C = 154;

        ArrayList<Parametro> listaParams = leerParametros();

        String vectorAlfa = "[";

        ArrayList<Solicitud> listaSolicitudes = new ArrayList();
        try {

            FileReader fr = new FileReader("solicitudesprueba.txt");
            String[] edgeText;

            try (BufferedReader entrada = new BufferedReader(fr)) {
                String line;
                while ((line = entrada.readLine()) != null) {
                    edgeText = line.split("\t");
                    Solicitud sol = new Solicitud(edgeText[0], edgeText[1], Double.parseDouble(edgeText[2]));
                    listaSolicitudes.add(sol);
                }
            }

            Double ftotal = 0.0;

            int cont = 0;
            System.out.println("DEBUG: indiceCaminosElegidos -> " + indiceCaminosElegidos.toString());
            System.out.println("DEBUG: listaSolicitudes -> " + listaSolicitudes.toString());
            for (Iterator<Solicitud> it = listaSolicitudes.iterator(); it.hasNext();) {
                //if (cont == indiceCaminosElegidos.size()) break;
                Solicitud solicitud = it.next();

                System.out.print("Origen: " + solicitud.getOrigen() + "\t"
                        + "Destino:" + solicitud.getDestino() + "\t"
                        + "FS requerida:" + solicitud.getVelocidad() + "\n");
                //calcularCaminosMasCortos(directedGraph, solicitud.getOrigen(), solicitud.getDestino(), contadorEscritura);
                ArrayList<Double> listaDistancias = leerDistanciasTodasSolicitudes();
                Double tsd;

                //for (int cont = 0; cont < indiceCaminosElegidos.size(); cont++) {
                System.out.println("DEBUG: cont -> " + cont);
                Double dist = listaDistancias.get(indiceCaminosElegidos.get(cont) - 1);
                //if(indiceCaminosElegidos.get(cont) == cont + 1) {
                cont = cont + 1;
                System.out.println("Distancia de camino " + cont + ": " + dist);
                int nivelModulacionCorrespondiente = leerNivelModulacionCorrespondiente(dist, listaParams);
                System.out.println("Nivel de Modulación correspondiente es " + nivelModulacionCorrespondiente);
                System.out.println("alfa = " + Math.ceil(solicitud.getVelocidad() * C / (nivelModulacionCorrespondiente * C)));
                Double d = Math.ceil(solicitud.getVelocidad() * C / (nivelModulacionCorrespondiente * C));

                if (it.hasNext()) {
                    vectorAlfa = vectorAlfa + d.intValue() + ",";
                } else {
                    vectorAlfa = vectorAlfa + d.intValue();
                }
                tsd = Math.ceil(solicitud.getVelocidad() * C / C);
                System.out.println("Tsd = " + tsd);
                ftotal += tsd;
                //}
                //}
            }

            vectorAlfa = vectorAlfa + "];";

            //System.out.println("vectorAlfa = " + vectorAlfa);
            System.out.println("ftotal = " + ftotal / C + ";");

        } catch (FileNotFoundException ex) {
            System.out.println("Archivo no encontrado: " + ex);
            Logger
                    .getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        }


        return vectorAlfa;
    }

    private static ArrayList<Parametro> leerParametros() {

        ArrayList<Parametro> listaParametros = new ArrayList();

        try {

            FileReader fr = new FileReader("parametros.txt");
            String[] edgeText;

            try (BufferedReader entrada = new BufferedReader(fr)) {
                String line;
                while ((line = entrada.readLine()) != null) {
                    edgeText = line.split("\t");
                    Parametro param = new Parametro(Double.parseDouble(edgeText[0]), Integer.parseInt(edgeText[1]));
                    listaParametros.add(param);
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Archivo no encontrado: " + ex);
            Logger
                    .getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return listaParametros;
    }

    private static Configuracion leerConfiguraciones() {

        Configuracion config = null;

        try {

            FileReader fr = new FileReader("configuraciones.txt");
            String texto = new String();
            String configText[];

            try (BufferedReader entrada = new BufferedReader(fr)) {
                String line;
                while ((line = entrada.readLine()) != null) {
                    texto += line + "\t";
                }
                configText = texto.split("\t");
                config = new Configuracion(Integer.parseInt(configText[0]), Integer.parseInt(configText[1]), Integer.parseInt(configText[2]));

            }
        } catch (FileNotFoundException ex) {
            System.out.println("Archivo no encontrado: " + ex);
            Logger
                    .getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return config;
    }

    private static ArrayList<Double> leerDistancias() {

        ArrayList<Double> listaDistancias = new ArrayList();
        try {

            FileReader fr = new FileReader("kshortestpath.txt");
            String[] edgeText;

            try (BufferedReader entrada = new BufferedReader(fr)) {
                String line;
                while ((line = entrada.readLine()) != null) {
                    edgeText = line.split("\t");
                    Double dist = Double.parseDouble(edgeText[edgeText.length - 1]);
                    listaDistancias.add(dist);
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Archivo no encontrado: " + ex);
            Logger
                    .getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return listaDistancias;
    }

    private static ArrayList<Double> leerDistanciasTodasSolicitudes() {

        ArrayList<Double> listaDistancias = new ArrayList();
        try {

            FileReader fr = new FileReader("kshortestpathCompleto.txt");
            String[] edgeText;

            try (BufferedReader entrada = new BufferedReader(fr)) {
                String line;
                while ((line = entrada.readLine()) != null) {
                    edgeText = line.split("\t");
                    Double dist = Double.parseDouble(edgeText[edgeText.length - 1]);
                    listaDistancias.add(dist);
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Archivo no encontrado: " + ex);
            Logger
                    .getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return listaDistancias;
    }

    private static int leerNivelModulacionCorrespondiente(Double distancia, ArrayList<Parametro> listaParams) {
        int nivMod = 1;

        for (Parametro parametroMod : listaParams) {
            if (distancia <= parametroMod.getDistanciaMaxima()) {
                nivMod = parametroMod.getNivelModulacion();
            }
        }
        return nivMod;
    }

    private static ArrayList<ArrayList<String>> leerTotalCaminos() {

        ArrayList<ArrayList<String>> listaCaminos = new ArrayList();

        try {
            FileReader fr = new FileReader("kshortestpathCompleto.txt");
            String[] edgeText;

            try (BufferedReader entrada = new BufferedReader(fr)) {
                String line;
                while ((line = entrada.readLine()) != null) {
                    ArrayList<String> listaEnlaces = new ArrayList();
                    edgeText = line.split("\\)");
                    String dist;
                    for (int i = 0; i < edgeText.length - 1; i++) {
                        dist = edgeText[i].trim().substring(1);
                        listaEnlaces.add(dist);
                    }
                    listaCaminos.add(listaEnlaces);
                }

            }
        } catch (FileNotFoundException ex) {
            System.out.println("Archivo no encontrado: " + ex);
            Logger
                    .getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return listaCaminos;
    }

    private static ArrayList<Integer> leerSalidaCplex(int k, int cantTotalCaminos, int cantSolicitudes) throws FileNotFoundException {
        ArrayList<Integer> listaCaminosElegidos = new ArrayList();
        FileReader fr = new FileReader("salidaCplexSP-ILP_1.txt");
        String[] edgeText;
        String texto = "";
        
        try (BufferedReader entrada = new BufferedReader(fr)) {
            String line;

            while ((line = entrada.readLine()) != null) {
                texto += line;
            }
            entrada.close();
        } catch (IOException ex) {
            Logger.getLogger(SPILP.class.getName()).log(Level.SEVERE, null, ex);
        }

        edgeText = texto.split("x = ");
        String texto2 = edgeText[1];
        //System.out.println("texto2 = " + texto2);
        texto2 = texto2.replace("]             [", "\n").replace(" ", "").replace("[", "").replace("]", "").replace(";", "");
        System.out.println("x = \n" + texto2);

        String[] textoZ = texto2.split("\n");

        for (int i = 0; i < textoZ.length; i++) {
            for (int j = 0; j < textoZ[i].length(); j++) {
                if (textoZ[i].charAt(j) == '1') {
                    listaCaminosElegidos.add((i + 1) * k - k + j + 1);
                    break;
                }
            }
        }
        return listaCaminosElegidos;
    }

    private static ArrayList<ArrayList<String>> leerSoloCaminosElegidos(ArrayList<Integer> indiceCaminosElegidos) {
        ArrayList<ArrayList<String>> listaCaminosElegidos = new ArrayList();
        try {
            FileReader fr = new FileReader("kshortestpathCompleto.txt");
            String[] edgeText;

            try (BufferedReader entrada = new BufferedReader(fr)) {
                String line;
                int contador = 1;
                while ((line = entrada.readLine()) != null) {
                    if (indiceCaminosElegidos.contains(contador)) {
                        ArrayList<String> listaEnlaces = new ArrayList();
                        edgeText = line.split("\\)");
                        String dist;
                        for (int i = 0; i < edgeText.length - 1; i++) {
                            dist = edgeText[i].trim().substring(1);
                            listaEnlaces.add(dist);
                        }
                        listaCaminosElegidos.add(listaEnlaces);
                    }
                    contador = contador + 1;
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Archivo no encontrado: " + ex);
            Logger
                    .getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SPILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return listaCaminosElegidos;
    }
    
    private static void  capaVirtual() throws 
            IOException{
        //en El route.dat tres dagtos el cual tenes escribir en el y salidaRout.txt tenemos que leer
        // n nodos, t tamanho de sltos en GHZ, d demanda, C Listado de capacitados,S listados de nodos inicios,E listados de nodos fin
        crearRouteData();
         long time_start1, time_end1;
                String argumentos[] = {"-v", "route.mod", "route.dat", "salidaRoute.txt"};
                time_start1 = System.currentTimeMillis();
                OplRunILP.main(argumentos);
                time_end1 = System.currentTimeMillis();
                System.out.println("La cota de la EON ha tomado " + (time_end1 - time_start1) + " milisegundos");
                String text = leerArchivo("salidaRoute.txt");
                String flujo = text.split("enlaceUsado")[0].split("=")[1];
                String enlacesUsados = text.split("=")[2];
                String[] enlacesUsados2 = flujo.split("]");
                 int i = 1;
                  FileWriter fw2 = new FileWriter("solicitudesprueba.txt");
            BufferedWriter bw2 = new BufferedWriter(fw2);
             try (PrintWriter salida2 = new PrintWriter(bw2)) {
                for(String pru : enlacesUsados2){
                 String[] prueba = pru.split("\\[");
                 if(prueba.length > 1){
                  for(String num : prueba){
                      String [] s = num.split(" ");
                      if(s.length>0){
                          int j = 1;
                           for (String var: s){
                          if(isNumeric(var)){
                           double numero = Double.parseDouble(var);
                           if(numero != 0){
                               salida2.println(i+"\t"+j+"\t"+var);
                               System.out.println(i+"\t"+j+"\t"+var);
                           }
                          
                          j = j+1;
                          }
                      }
                      }
                     
                  }
                  i = i +1;
                 }
                }
                 }
           }
    
   
    
    private static void crearRouteData(){
         ArrayList<Solicitud> listaSolicitudes = new ArrayList();
        try {

            FileReader fr = new FileReader("solicitudes2.txt");
            Integer numeroNodo = 10;
            Double  ghzF = 12.5;
            String[] edgeText;

            try (BufferedReader entrada = new BufferedReader(fr)) {
                String line;
                while ((line = entrada.readLine()) != null) {
                    edgeText = line.split("\t");
                    Solicitud sol = new Solicitud(edgeText[0], edgeText[1], Double.parseDouble(edgeText[2]));
                    listaSolicitudes.add(sol);
                }
            }
            FileWriter fw2 = new FileWriter("route.dat");
            BufferedWriter bw2 = new BufferedWriter(fw2);
            
                try (PrintWriter salida2 = new PrintWriter(bw2)) {
                    //n Cantidad de numeros de nodos dentro de la red
                    System.out.println("n = " + numeroNodo + ";");
                     salida2.println("n = " + numeroNodo + ";");
                     //t tamaño de  los sltos
                     System.out.println("t = " + ghzF + ";");
                    salida2.println("t = " + ghzF + ";");
                    //d Cantidad de demandas
                    System.out.println("d = " + listaSolicitudes.size() + ";");
                     salida2.println("d = " + listaSolicitudes.size() + ";");
                   // C S E
                   //C GBPS de cada demanda
                    System.out.print("C = [");
                    salida2.print("C = [");
                    for (Iterator<Solicitud> it = listaSolicitudes.iterator(); it.hasNext();) {
                        Solicitud solucitud = it.next();
                       System.out.print(solucitud.getVelocidad());
                        salida2.print(solucitud.getVelocidad());
                       if (it.hasNext()) {
                            System.out.print(",");
                            salida2.print(",");
                        } else {
                            System.out.println("];");
                            salida2.println("];");
                        }
                    }
                     //S nodo fuente de cada demanda
                    System.out.print("S = [");
                    salida2.print("S = [");
                    for (Iterator<Solicitud> it = listaSolicitudes.iterator(); it.hasNext();) {
                        Solicitud solucitud = it.next();
                       System.out.print(solucitud.getOrigen());
                        salida2.print(solucitud.getOrigen());
                       if (it.hasNext()) {
                            System.out.print(",");
                            salida2.print(",");
                        } else {
                            System.out.println("];");
                            salida2.println("];");
                        }
                    }
                     //E no fin de cada demanda
                    System.out.print("E = [");
                    salida2.print("E = [");
                    for (Iterator<Solicitud> it = listaSolicitudes.iterator(); it.hasNext();) {
                        Solicitud solucitud = it.next();
                       System.out.print(solucitud.getDestino());
                        salida2.print(solucitud.getDestino());
                       if (it.hasNext()) {
                            System.out.print(",");
                            salida2.print(",");
                        } else {
                            System.out.println("];");
                            salida2.println("];");
                        }
                    }
                }

               

            

        } catch (FileNotFoundException ex) {
            System.out.println("Archivo no encontrado: " + ex);
            Logger
                    .getLogger(SLILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SLILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

    
    
    }

    private static String leerArchivo(String nombreArchivo) {
        String texto = new String();
        try {
            FileReader fr = new FileReader(nombreArchivo);
            try (BufferedReader entrada = new BufferedReader(fr)) {
                String line;
                int lineaLectura = 0;
                while ((line = entrada.readLine()) != null) {
                    lineaLectura = lineaLectura + 1;
                    if (lineaLectura >= 11) {
                        texto += line + "\n";
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Archivo no encontrado: " + ex);
            Logger
                    .getLogger(SLILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SLILP.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return texto;
    }
    
    
}
