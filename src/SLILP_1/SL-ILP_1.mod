/*********************************************
 * OPL 12.6.0.0 Model
 * Author: ACER
 * Creation Date: 31/10/2016 at 16:33:50
 *********************************************/

//constantes
int SD = ...;
int ENLACES = ...;
int VERTICES = ...;
range R = 1..3;
range sd = 1..SD;
range vertices = 1..VERTICES;
range mn = 1..ENLACES;
int alfa[sd][R]=...;
//int SOLBIN[vertices][1..2]=...;
int LISTAVERTICES[vertices]=...;
int SOLICITUDES[sd][1..2]=...;
float Lmax[R] = ...;
int F = ...;
int GB = ...;
float L[mn] = ...;
int NE[vertices][mn]=...;
int NE1[vertices][mn]=...;
int NE2[vertices][mn]=...;

//variables
dvar int+ FL[mn];//8
dvar int+ N[sd][R];//12*3=36
dvar int+ Z[sd][R];//12*3=36
dvar int+ NL[sd][R][mn];//12*3*8=288
dvar int Y[sd][R][mn] in 0..1;//12*3*8=288

dvar float c; // 1

minimize c;

subject to {
        forall(mn1 in mn) {//8
                //c >= FL[mn1];
                c >= FL[mn1] + 1;               
                FL[mn1] == sum(s_d in sd, r in R) NL[s_d][r][mn1];
        }

        /* 1- Single path routing constraint */
        forall(s_d in sd, i in vertices) {//12
        	if(LISTAVERTICES[i] == SOLICITUDES[s_d][1]) {
                        ctSinglePathRouting2:
                        sum(r in R, m_n in mn: NE1[i][m_n] == 1) Y[s_d][r][m_n] == 1;
      		}        
        }
        
        forall(s_d in sd, i in vertices) {//12
        	if(LISTAVERTICES[i] == SOLICITUDES[s_d][1]) {
                        ctSinglePathRouting2_2:
                        sum(r in R,m_n in mn: NE2[i][m_n] == 1) Y[s_d][r][m_n] == 0;
        	}        
        }

        forall(s_d in sd, i in vertices) {//12
        	if(LISTAVERTICES[i] == SOLICITUDES[s_d][2]) {
                        ctSinglePathRouting3:
                        sum(r in R,m_n2 in mn: NE2[i][m_n2] == 1) Y[s_d][r][m_n2] == 1;
      		}
        }
        
        forall(s_d in sd, i in vertices) {//12
        	if(LISTAVERTICES[i] == SOLICITUDES[s_d][2]) {
                        ctSinglePathRouting3_2:
                        sum(r in R, m_n2 in mn: NE1[i][m_n2] == 1) Y[s_d][r][m_n2] == 0;
       		}        	
        }
       
        

        forall(s_d in sd, i in vertices, r in R) {//12*4*3=144
                                
                	if(LISTAVERTICES[i] != SOLICITUDES[s_d][2] && LISTAVERTICES[i] != SOLICITUDES[s_d][1]) {
                        ctSinglePathRouting4_1:
                        (sum(m_n1 in mn: NE1[i][m_n1] == 1) Y[s_d][r][m_n1]) - 
                        (sum(m_n2 in mn: NE2[i][m_n2] == 1) Y[s_d][r][m_n2]) == 0;
                    } 
                                        
        } 
        forall(s_d in sd, i in vertices, r in R) {//12*4*3*2=288  
        	ctSinglePathRouting4_2:
            (sum(m_n1 in mn: NE1[i][m_n1] == 1) Y[s_d][r][m_n1]) <= 1;
            (sum(m_n2 in mn: NE2[i][m_n2] == 1) Y[s_d][r][m_n2]) <= 1; 
                                        
        } /* con esta restricción se evitan los ciclos */
        

        /* 2- Flow conservation constraint */


        forall(s_d in sd) {//12*3=36
                forall(r in R, i in vertices) {
                	if(LISTAVERTICES[i] == SOLICITUDES[s_d][1]) {  
	                	ctFlowConservationConstraint1:
	                        sum(m_n in mn: NE1[i][m_n] == 1) NL[s_d][r][m_n] == N[s_d][r];               
	                }
                        
                }
        }
        
        forall(s_d in sd) {//12*3=36
                forall(r in R, i in vertices) {
                	if(LISTAVERTICES[i] == SOLICITUDES[s_d][1]) {  
                        ctFlowConservationConstraint1_2:
                        sum(m_n in mn: NE2[i][m_n] == 1) NL[s_d][r][m_n] == 0;
                    }
                }
        }

        forall(s_d in sd) {//12*3=36
                forall(r in R, i in vertices) {
                	if(LISTAVERTICES[i] == SOLICITUDES[s_d][2]) {       
                		ctFlowConservationConstraint2:
                        sum(m_n in mn: NE2[i][m_n] == 1) NL[s_d][r][m_n] == N[s_d][r];         
                	}       
                }
        }
        
        forall(s_d in sd) {//12*3=36
                forall(r in R, i in vertices) {                    
                	if(LISTAVERTICES[i] == SOLICITUDES[s_d][2]) {
                        ctFlowConservationConstraint2_2:
                        sum(m_n in mn: NE1[i][m_n] == 1) NL[s_d][r][m_n] == 0;
              		}                
                }
        }

 		forall(s_d in sd, i in vertices,r in R) {//12*4*3=144
                 
                	if(LISTAVERTICES[i] != SOLICITUDES[s_d][2] && LISTAVERTICES[i] != SOLICITUDES[s_d][1]) {
                        ctSinglePathRouting4_1_2:
                        sum(m_n in mn: NE1[i][m_n] == 1) NL[s_d][r][m_n] -
                        sum(m_n in mn: NE2[i][m_n] == 1) NL[s_d][r][m_n] == 0;
                    }                     
        } 
        /* 3- Starting Frequencies ordering constraint */

//        forall(t in sd){
//                forall(u in sd){
//                        if(t != u){
//                                ctStartingFrequenciesOrderingConstraint:
//                                X[t][u] + X[u][t] == 1;
//                        }
//                }
//        }

        /* 4- Spectrum continuity and non-overlapping spectrum
           allocation constraints */
//        forall(t in sd){
//                forall(u in sd){
//                        forall(m_n in mn, r in R, r2 in R){
//                                if(t != u)
//                                        ctSpectrumContinuity:
//                                        E[u] - S[t] <= F * (X[t][u] + 2 - Y[t][r][m_n] - Y[u][r2][m_n]) - GB - 1;
//                        }
//                }
//        }

        /* 5- Physical Link Usage Constraint */

        forall(s_d in sd, r in R, mn in mn){//12*3*8*2=576
                ctPhysicalLinkUsage:
                NL[s_d][r][mn] <= Y[s_d][r][mn] * F;
                Y[s_d][r][mn] <= NL[s_d][r][mn];/*agrego para corregir inconsistencia*/
        }

        /* 6- Spectrum path Modulation Format constraint */

        forall(s_d in sd,r in R){//12*3=36
                        ctSpectrumPathModulation:
                        N[s_d][r] <= Z[s_d][r] * F;
        }

        /* 7- Modulation Format Constraint */

        forall(s_d in sd) {//12
                ctModulationFormatConstraint:
                sum(r in R) Z[s_d][r] == 1;
        };


        /* 8- Transmission reach constraint */
        forall(s_d in sd, r in R){//12*3=36
                ctTransmissionReachConstraint:
                sum(mn2 in mn) (Y[s_d][r][mn2] * L[mn2]) <= Lmax[r];
        }

        /* 9- Max_FS_ID Constraint */
//        forall(mn1 in mn) {
//        	forall(s_d in sd){
//        		forall(r in R) {
//        	  
//	                ctMax_FS_IDConstraint:
//	                MI[mn1] >= E[s_d] - F * (1 - Y[s_d][r][mn1]);
//	             }
//            }                            
//        }

//        /* 10- Others... */

        forall(s_d in sd, r in R) {//12*3=36
                        ctOthers01:
                        N[s_d][r] == Z[s_d][r] * alfa[s_d][r];
        }
        /*forall(s_d in sd){
                ctOthers02:
                E[s_d] == S[s_d] + NFS[s_d] - 1;
        }*/

        /*forall(s_d in sd){
                ctOthers03:
                NFS[s_d] == sum(r in R) N[s_d][r];
        }*/
}