/*********************************************
 * OPL 12.6.0.0 Model
 * Author: ACER
 * Creation Date: 01/11/2016 at 16:30:10
 *********************************************/
/*
//constantes
int SD = ...;
int ENLACES = ...;
int VERTICES = ...;
range R = 1..3;
range sd = 1..SD;
range vertices = 1..VERTICES;
range mn = 1..ENLACES;
int SOLICITUDES[sd][1..2]=...;
int F = ...;
int GB = ...;
int NE[vertices][mn]=...;
int NE1[vertices][mn]=...;
int NE2[vertices][mn]=...;
int Z[sd][R] =...;
int Y[sd][R][mn] =...;
int N[sd][R] =...;
int NL[sd][R][mn] = ...;
int FL[mn] = ...;

//variables
dvar int+ MI[mn];//8
dvar int X[sd][sd] in 0..1;//12*12=144
dvar int+ E[sd];//12
dvar int+ S[sd];//12
dvar int+ NFS[sd];//12
dvar float c;//1

minimize c;

subject to {
        forall(mn1 in mn) {//8
                //c >= MI[mn1];
                c >= MI[mn1] + 1;
        }

        /* 2- Flow conservation constraint */


        /*forall(s_d in sd) {
                forall(r in R) {
                        ctFlowConservationConstraint1:
                        sum(m_n in mn: NE1[SOLICITUDES[s_d][1]][m_n] == 1) NL[s_d][r][m_n]
== N[s_d][r];
                }
        }
        
        forall(s_d in sd) {
                forall(r in R) {
                        ctFlowConservationConstraint1_2:
                        sum(m_n in mn: NE2[SOLICITUDES[s_d][1]][m_n] == 1) NL[s_d][r][m_n]
== 0;
                }
        }

        forall(s_d in sd) {
                forall(r in R) {
                        ctFlowConservationConstraint2:
                        sum(m_n in mn: NE2[SOLICITUDES[s_d][2]][m_n] == 1) NL[s_d][r][m_n] == N[s_d][r];
                }
        }
        
        forall(s_d in sd) {
                forall(r in R) {
                        ctFlowConservationConstraint2_2:
                        sum(m_n in mn: NE1[SOLICITUDES[s_d][2]][m_n] == 1) NL[s_d][r][m_n] == 0;
                }
        }

 		forall(s_d in sd, i in vertices,r in R) {
                 
                	if(i != SOLICITUDES[s_d][2] && i != SOLICITUDES[s_d][1]) {
                        ctSinglePathRouting4_1_2:
                        sum(m_n in mn: NE1[i][m_n] == 1) NL[s_d][r][m_n] -
                        sum(m_n in mn: NE2[i][m_n] == 1) NL[s_d][r][m_n] == 0;
                    }                     
        }*/ 
        
        /* 3- Starting Frequencies ordering constraint */

       /* forall(t in sd){//12*12=144
                forall(u in sd){
                        if(t != u){
                                ctStartingFrequenciesOrderingConstraint:
                                X[t][u] + X[u][t] == 1;
                        }
                }
        }*/

        /* 4- Spectrum continuity and non-overlapping spectrum
           allocation constraints */
       /* forall(t in sd){//12*12*8*3*3=10368
                forall(u in sd){
                        forall(m_n in mn, r in R, r2 in R){
                                if(t != u)
                                        ctSpectrumContinuity:
                                        E[u] - S[t] <= F * (X[t][u] + 2 - Y[t][r][m_n] - Y[u][r2][m_n]) - GB - 1;
                        }
                }
        }*/

        /* 5- Physical Link Usage Constraint */
/*
        forall(s_d in sd, r in R, mn in mn){//12*3*8=288
                ctPhysicalLinkUsage:
                NL[s_d][r][mn] <= Y[s_d][r][mn] * F;
        }

    */    
        /* 9- Max_FS_ID Constraint */
 /*       forall(mn1 in mn) {//8*12*3=288
        	forall(s_d in sd){
        		forall(r in R) {
        	  
	                ctMax_FS_IDConstraint:
	                MI[mn1] >= E[s_d] - F * (1 - Y[s_d][r][mn1]);
	             }
            }                            
        }
*/
        /* 10- Others... */

        /*forall(s_d in sd){
                forall(r in R){
                        ctOthers01:
                        N[s_d][r] == Z[s_d][r] * alfa[s_d][r];
                }
        }*/
 /*       
        forall(s_d in sd){//12
                ctOthers02:
                E[s_d] == S[s_d] + NFS[s_d] - 1;
        }

        forall(s_d in sd){//12
                ctOthers03:
                NFS[s_d] == sum(r in R) N[s_d][r];
        }
}
*/



 int SD = ...;
 float Ftotal = ...;
 int G = ...;
 //int s2d2 = ...;
 
 //variables

 range sd = 1.. SD;
 //range S2D2 = 1 .. s2d2;
 int l[sd][sd] = ...;
 
 int alfa[sd]=...;
 
 dvar int+ f[sd];
 
 dvar int delta[sd][sd] in 0..1;

 
 dvar int+ c;
 
 
 
 minimize c;
 
 
 subject to {
 	forall(s_d in sd) {
 	 	c >= f[s_d] + alfa[s_d]; 	 	
 	};
 	 
 	/*
 	forall(s_d in sd) {
 		sum(p in p) x[s_d][p] == 1;
	};	
 	*/
	
	forall(t in sd){
		forall(u in sd){
			if((l[t][u] == 1) && (t != u)) 
 		 	{
 		 		delta[t][u] + delta[u][t] == 1;
 		 		f[u]-f[t] <= Ftotal * delta[t][u];
 		 		f[t]-f[u] <= Ftotal * delta[u][t];
 		 	}			 		
 		} 		 	
	}; 
	
	forall(t in sd){
		forall(u in sd){
			if((l[t][u] == 1) && (t != u)) 
	 		{
	 			f[t] + alfa[t] + G - f[u] <= 
	 						(Ftotal + G) * (1 - delta[t][u]);
				f[u] + alfa[u] + G - f[t] <= 
							(Ftotal + G) * (1 - delta[u][t]);     
			}							 					
		}
	};
	
	forall(s_d in sd){
			f[s_d] <= Ftotal-1;
	}
}